#include <iostream>
template<class T>
class linklist{
private:
	struct node{
		T value;
		node *next;
	};
	node* head;
public:
	~linklist(){
		clear();
	}
void push_front(int number){
	if (!head){
		head = new node;
		head->value = number;
		head->next = nullptr;
	}
	else{
		node *temp = new node;
		temp->next = head;
		temp->value = number;
		head = temp;
	}
}
void push_back(int number){
	if (!head){
		head = new node;
		head->value = number;
		head->next = nullptr;
	}
	else{
		node *temp;
		for (temp = head; temp->next; temp = temp->next);
		temp->next = new node;
		temp->next->value = number;
		temp->next->next = nullptr;
	}
}
void pop_front(){
	if (!head)
		std::cout << "list empty" << std::endl;
	else{
		node *temp;
		temp = head->next;
		delete head;
		head = temp;
	}
}
void pop_back(){
	if (!head)
		std::cout << "list empty " << std::endl;
	else{
		if (head->next){
			node *temp;
			temp = head;
			for (; temp->next->next; temp = temp->next);
			delete temp->next;
			temp->next = nullptr;
		}
		else{
			delete head;
			head = nullptr;
		}
	}
}
void clear(){
	if (!head)
		std::cout << "the list is empty" << std::endl;
	else{
		while (head != NULL){
			pop_back();
		}
		delete head;
		head = nullptr;
	}
}
int size(){
	int size = 1;
	if (!head){
		std::cout << "list empty" << std::endl;
		size = 0;
	}
	else{
		node *temp;
		temp = head;
		while (temp->next != NULL){
			temp = temp->next;
			size++;
		}
	}
	return size;
}
bool find(int number){
	if (!head)
		std::cout << "list empty" << std::endl;
	else{
		node *temp;
		node *current;
		node *searchnumber;
		temp = head;
		current = head;
		while (current != NULL && current->value != number){
			temp = current;
			current = current->next;
		}
		if (current != NULL){
			searchnumber = current;
			current = current->next;
			std::cout << searchnumber->value << " Found" << std::endl;
			return true;
		}
		else{
			std::cout << "Not found" << std::endl;
			return false;
		}
	}
}
};