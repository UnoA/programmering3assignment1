#include "linklist.h"
#include "binarytree.h"
#include "vld.h"
#include "unit.h"
int main(){	
	int EnteredNumber = 0;
	std::cout << "Press 1 to Test LinkedList or Press 2 to Test Binary Search Tree" << std::endl;
	std::cin >> EnteredNumber;
	std::cout << "\n" << std::endl;
	if (EnteredNumber == 1){
		linklist<int>* linkedlist = new linklist<int>();
		linkedlist->push_front(13);
		linkedlist->push_back(16);
		linkedlist->push_back(19);
		linkedlist->push_front(24);
		linkedlist->push_front(11);
		linkedlist->push_back(16);
		linkedlist->pop_front();
		linkedlist->pop_back();
		verify(4, linkedlist->size(), "Testing a list with 4 elements");
		std::cout << "\n" << std::endl;
		verify(true, linkedlist->find(16), "searching for 16 that is in the list");
		std::cout << "\n" << std::endl;
		verify(false, linkedlist->find(20), "searching for 20 that is not in the list");
		std::cout << "\n" << std::endl;
		linkedlist->clear();
		verify(0, linkedlist->size(), "Testing an empty list");
		delete linkedlist;
		linkedlist = nullptr;
	}
	if (EnteredNumber == 2){
		BSTnode<int>* BST = new BSTnode<int>();
		BST->Insert(15);
		BST->Insert(20);
		BST->Insert(10);
		BST->Insert(17);
		BST->Insert(21);
		BST->Insert(13);
		BST->Insert(16);
		BST->Insert(24);
		BST->Insert(11);
		BST->traversal_pre_order();
		std::cout << "\n" << std::endl;
		BST->traversal_in_order();
		std::cout << "\n" << std::endl;
		BST->traversal_post_order();
		std::cout << "\n" << std::endl;
		verify(9, BST->size(), "Testing a tree with 9 elements");
		std::cout << "\n" << std::endl;
		verify(13, BST->find(13), "searching for number 13 that is in the tree");
		std::cout << "\n" << std::endl;
		verify(18, BST->find(18), "searching for number 18 that is not in the tree");
		BST->clear();
		std::cout << "\n" << std::endl;
		verify(0, BST->size(), "Testing an emprty tree");
		delete BST;
		BST = nullptr;
	}
	system("pause");
	return 0;
}