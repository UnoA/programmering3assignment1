#include <iostream>
template<class T>
class BSTnode{
private:
	struct Node{
		T data;
		Node* left;
		Node* right;
	};
public:
	~BSTnode(){
		clear();
	}
	void Insert(int data){
		if (!root){
			root = new Node();
			root->data = data;
			root->left = nullptr;
			root->right = nullptr;
		}
		else{
			GetNewNode(root, data);
		}
	}
	void clear(){
		clear_2(root);
		delete root;
		root = nullptr;
	}
	int size(){
		return size_2(root);
	}
	int find(T data){
		find_2(root, data);
		return data;
	}
	void traversal_pre_order()
	{
		traversal_pre_order_2(root);
	}
	void traversal_in_order()
	{
		traversal_in_order_2(root);
	}
	void traversal_post_order()
	{
		traversal_post_order_2(root);
	}
private:
	void GetNewNode(Node* next, T data){
		if (next->data > data){
			if (!next->left){
				next->left = new Node();
				next->left->data = data;
			}
			else
				GetNewNode(next->left, data);
		}
		else{
			if (next->data < data){
				if (!next->right){
					next->right = new Node();
					next->right->data = data;
				}
				else
					GetNewNode(next->right, data);
			}
		}
	}
	void traversal_pre_order_2(Node* node){
		if (node){
			std::cout << node->data << std::endl;
			traversal_pre_order_2(node->left);
			traversal_pre_order_2(node->right);
			
		}
	}
	void traversal_in_order_2(Node* node){
		if (node){
		traversal_in_order_2(node->left);
		std::cout << node->data << std::endl;
		traversal_in_order_2(node->right);
		}
	}
	void traversal_post_order_2(Node* node){
		if (node){
		traversal_post_order_2(node->left);
		traversal_post_order_2(node->right);
		std::cout << node->data << std::endl;
		}
	}
	bool find_2(Node* root, T data){
		if (root == NULL)
			return false;
		else if (root->data == data)
			return true;
		else if (data <= root->data)
			return find_2(root->left, data);
		else
			return find_2(root->right, data);
	}
	int size_2(Node* root){
		if (root == NULL)
			return false;
		else
			return size_2(root->left) + size_2(root->right) +1;
	}
	void clear_2(Node* node){
		if (node != NULL){
			if (node->left != NULL){
				clear_2(node->left);
				delete node->left;
				node->left = nullptr;
			}
			 if (node->right != NULL){
				clear_2(node->right);
				delete node->right;
				node->right = nullptr;
			}
		}
	}
	Node* root;
};